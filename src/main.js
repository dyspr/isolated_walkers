var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0],
  medium: [128, 128, 128],
  walkers: [
    [16, 16, 16],
    [128, 128, 128],
    [255, 255, 255],
    [32, 32, 32],
    [64, 64, 64]
  ]
}

var arrays = [create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true), create2DArray(16, 16, 0, true)]
var walkers = [{
    x: 0,
    y: 0
  },
  {
    x: 15,
    y: 0
  },
  {
    x: 0,
    y: 15
  },
  {
    x: 15,
    y: 15
  },
  {
    x: 7,
    y: 7
  }
]
var neighbours = []
var frames = 0
var boardSize

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  frameRate(60)
  // background rectangle
  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)
  // grid points
  for (var i = 0; i < 17; i++) {
    for (var j = 0; j < 17; j++) {
      fill(colors.medium)
      noStroke()
      ellipse(windowWidth * 0.5 + (i - 8) * (42 / 768) * boardSize, windowHeight * 0.5 + (j - 8) * (42 / 768) * boardSize, 3, 3)
    }
  }
  // drawing random walker
  for (var i = 0; i < arrays.length; i++) {
    for (var j = 0; j < arrays[i].length; j++) {
      for (var k = 0; k < arrays[i][j].length; k++) {
        fill(colors.walkers[i][0], colors.walkers[i][1], colors.walkers[i][2], 1)
        noStroke()
        rect(windowWidth * 0.5 + (j - 7.5) * (42 / 768) * boardSize, windowHeight * 0.5 + (k - 7.5) * (42 / 768) * boardSize, arrays[i][j][k] * (32 / 768) * boardSize, arrays[i][j][k] * (32 / 768) * boardSize)
      }
    }
  }

  // updating random walker
  frames += deltaTime * 0.025
  if (frames > 1) {
    frames = 0

    for (var i = 0; i < walkers.length; i++) {
      arrays[i][walkers[i].x][walkers[i].y] = 1
      var neighbours = getNeighbours(walkers[i].x, walkers[i].y, arrays)
      var rand = Math.floor(Math.random() * neighbours.length)
      if (neighbours[rand] !== undefined) {
        walkers[i].x = neighbours[rand][0]
        walkers[i].y = neighbours[rand][1]
      }
      neighbours = []
    }
    for (var i = 0; i < arrays.length; i++) {
      for (var j = 0; j < arrays[i].length; j++) {
        for (var k = 0; k < arrays[i][j].length; k++) {
          if (arrays[i][j][k] > 0) {
            arrays[i][j][k] -= 0.025
          } else {
            arrays[i][j][k] = 0
          }
        }
      }
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function getNeighbours(x, y, arrays) {
  var neighbours = []
  if (arrays[0][(x + 1) % arrays[0].length][y] === 0 && arrays[1][(x + 1) % arrays[1].length][y] === 0 && arrays[2][(x + 1) % arrays[2].length][y] === 0 && arrays[3][(x + 1) % arrays[3].length][y] === 0 && arrays[4][(x + 1) % arrays[4].length][y] === 0) {
    neighbours.push([(x + 1) % arrays[0].length, y])
  }
  if (arrays[0][(arrays[0].length + (x - 1)) % arrays[0].length][y] === 0 && arrays[1][(arrays[1].length + (x - 1)) % arrays[1].length][y] === 0 && arrays[2][(arrays[2].length + (x - 1)) % arrays[2].length][y] === 0  && arrays[3][(arrays[3].length + (x - 1)) % arrays[3].length][y] === 0  && arrays[4][(arrays[4].length + (x - 1)) % arrays[4].length][y] === 0) {
    neighbours.push([(arrays[0].length + (x - 1)) % arrays[0].length, y])
  }
  if (arrays[0][x][(y + 1) % arrays[0].length] === 0 && arrays[1][x][(y + 1) % arrays[1].length] === 0 && arrays[2][x][(y + 1) % arrays[2].length] === 0 && arrays[3][x][(y + 1) % arrays[3].length] === 0 && arrays[4][x][(y + 1) % arrays[4].length] === 0) {
    neighbours.push([x, (y + 1) % arrays[0].length])
  }
  if (arrays[0][x][(arrays[0].length + (y - 1)) % arrays[0].length] === 0 && arrays[1][x][(arrays[1].length + (y - 1)) % arrays[1].length] === 0 && arrays[2][x][(arrays[2].length + (y - 1)) % arrays[2].length] === 0 && arrays[3][x][(arrays[3].length + (y - 1)) % arrays[3].length] === 0 && arrays[4][x][(arrays[4].length + (y - 1)) % arrays[4].length] === 0) {
    neighbours.push([x, (arrays[0].length + (y - 1)) % arrays[0].length])
  }
  return neighbours
}
